//
//  BioService.h
//  BioService
//
//  Created by 汪宁 on 2018/5/2.
//  Copyright © 2018年 WN. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BioService.
FOUNDATION_EXPORT double BioServiceVersionNumber;

//! Project version string for BioService.
FOUNDATION_EXPORT const unsigned char BioServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BioService/PublicHeader.h>

#import<BioService/TouchIdService.h>
