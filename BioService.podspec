Pod::Spec.new do |s|

  s.name         = "BioService"
  s.version      = "1.0.0"
  s.summary      = "BioService类库"

  s.description  = <<-DESC
                   * Detail about BioService framework.
                   DESC

  s.homepage     = "http://"
  s.license      = 'MIT (example)'
  s.author       = { "未定义" => "undefined" }
  s.platform     = :ios, '8.0'
  s.ios.deployment_target = '8.0'
  s.source       = { :http => "http://BioService.zip" }
  s.preserve_paths = "BioService.framework/*"
  s.resources  = "BioService.framework/*.{bundle,xcassets}"
  s.vendored_frameworks = 'Login.framework'
  s.requires_arc = true
  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/BioService' }

  #s.dependency 'XXXX'

end
